new d3pie("pieSN", {
	"header": {
		"title": {
			"text": "Composition politique du Sénat",
			"fontSize": 23,
			"font": "open sans"
		},
		"subtitle": {
			"text": "(inscrits, apparentés, rattachés au 22/02/2019)",
			"color": "#999999",
			"fontSize": 12,
			"font": "open sans"
		},
		"titleSubtitlePadding": 9
	},
	"footer": {
		"color": "#999999",
		"fontSize": 10,
		"font": "open sans",
		"location": "bottom-left"
	},
	"size": {
		"canvasWidth": 1000,
		"pieOuterRadius": "80%"
	},
	"data": {
		"sortOrder": "value-desc",
		"smallSegmentGrouping": {
			"enabled": true
		},
		"content": [
			{
				"label": "Groupe socialiste et républicain",
				"value": 74,
				"color": "#2484c1"
			},
			{
				"label": "Les Républicains",
				"value": 145,
				"color": "#0c6197"
			},
			{
				"label": "Groupe Union Centriste",
				"value": 51,
				"color": "#4daa4b"
			},
			{
				"label": "La République en Marche",
				"value": 23,
				"color": "#90c469"
			},
			{
				"label": "Socialistes et apparentés",
				"value": 22,
				"color": "#daca61"
			},
			{
				"label": "Rassemblement Démocratique et social européen",
				"value": 16,
				"color": "#e4a14b"
			},
			{
				"label": "Communiste républicain citoyen et écologiste",
				"value": 12,
				"color": "#e98125"
			},
			{
				"label": "Les Indépendants",
				"value": 5,
				"color": "#cb2121"
			},
		]
	},
	"labels": {
		"outer": {
			"pieDistance": 32
		},
		"inner": {
			"hideWhenLessThanPercentage": 3
		},
		"mainLabel": {
			"fontSize": 11
		},
		"percentage": {
			"color": "#ffffff",
			"decimalPlaces": 0
		},
		"value": {
			"color": "#adadad",
			"fontSize": 11
		},
		"lines": {
			"enabled": true
		},
		"truncation": {
			"enabled": true
		}
	},
	"effects": {
		"pullOutSegmentOnClick": {
			"effect": "linear",
			"speed": 400,
			"size": 8
		}
	}
});
