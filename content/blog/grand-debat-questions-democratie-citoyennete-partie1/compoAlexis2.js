// // Load the Observable runtime and inspector.
// import {Runtime, Inspector} from "https://unpkg.com/@observablehq/notebook-runtime?module";

// Your notebook, compiled as an ES module.
// import notebook from "https://api.observablehq.com/@jashkenas/my-neat-notebook.js";

// Or, your notebook, downloaded locally.
// import notebook from "./my-neat-notebook.js";

// Load the notebook, observing its cells with a default Inspector
// that simply renders the value of each cell into the provided DOM node.
// Runtime.load(notebook, Inspector.into(document.body));

// Runtime.load(notebook, function(cell) {
//   var div = document.createElement("div");
//   document.body.appendChild(div);
//   return new Inspector(div);
// });

Runtime.load(notebook, function(cell) {
    if (cell.name === "chart") {
        return {
            fulfilled: (value) => {
                document.getElementById("lol2").appendChild(value);
            }
        };
    }
});
